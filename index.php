<?php
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("shaun");
echo "Name : ".$sheep->name."<br>"; // "shaun"
echo "legs : ".$sheep->legs."<br>"; // 4
echo "Cold Blooded : ".$sheep->cold_blooded."<br> <br>"; // "no"

$kodok = new Frog("buduk");
echo "Name : ".$kodok->name."<br>";
echo "legs : ".$kodok->legs."<br>";
echo "Cold Blooded : ".$kodok->cold_blooded."<br>";
echo "Jump : ";
echo $kodok->jump()."<br> <br>" ; // "hop hop"

$sungokong = new Ape("kera sakti");
echo "Name : ".$sungokong->name."<br>";
echo "legs : ".$sungokong->legs."<br>";
echo "Cold  Blooded : ".$sungokong->cold_blooded."<br>";
echo "Yell : "; 
echo $sungokong->yell(); // "Auooo"


// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
